const mix = require('laravel-mix');
const themePath = 'wp-content/themes/medpagalba';

mix.sass(`${themePath}/sass/style.scss`, `${themePath}/assets/dist`)
    .setPublicPath(`${themePath}/assets/dist`);

mix.js([
    // 'js/jquery-3.3.1.min.js',
    // 'js/semantic.min.js',
    `${themePath}/js/main.js`,
], `${themePath}/assets/dist/app.js`);
