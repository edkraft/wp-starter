## Requirements
  * WP-CLI - https://wp-cli.org/#installing

Git repository only contains theme plugins folders and webpack file.
```
/wp-content/themes/YOUR_THEME
/wp-content/plugins/
package.json
webpack.mix.js
run.sh
```


## Setup locally
* Login to production site and export database with Migrate DB plugin.
* Import database locally

```sh
git clone YOUR_REPO PROJECT_FOLDER
```
```sh
cd PROJECT_FOLDER
```
```sh
cp run.sh.example run.sh
chmod +x run.sh
```
Open run.sh file and setup database variables:
```
DATABASE_NAME=
DATABASE_USER=
DATABASE_PASS=
PREFIX_=
```
After that run (this will download latest wordpress version and generates config.php)
```sh
./run.sh --install (Downloads WP core on empty project)
./run.sh --reinstall (Removes all WP core files and downloads new WP files)
./run.sh --reset (Removes all WP core files)
```

## Working with assets (css, js)
All css files is in: /wp-content/themes/medpagalba/sass/
There is webpack.mix.js, package.json files in root of the project.
Install npm packages:
```sh
npm install
```
Other commands:
```sh
npm run watch
npm run dev
npm run production
```


## If WP-CLI not working on MAMP
MAMP and WP-CLI use different php versions, to fix this open bash profile file in terminal:
```sh
nano ~/.bash_profile
```
Paste these 3 lines in top
```sh
export PATH=/Applications/MAMP/Library/bin:$PATH
PHP_VERSION=`ls /Applications/MAMP/bin/php/ | sort -n | tail -1`
export PATH=/Applications/MAMP/bin/php/${PHP_VERSION}/bin:$PATH
```
Reload bash
```sh
source ~/.bash_profile
```